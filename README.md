# Übersetzung und Formatierung

einfach die `index.php` ausführen.

### Ausgabe:

```
Währungen:
        Deutschland: 15,86 €
        Österreich: € 15,86
        England: £15.86
        USA: $15.86

Gewicht:
        Deutschland: 15,86 kg

Zahlenwerte:
        Deutschland: 15.897.915,869
        USA: 15,897,915.869
        Deutschland: neun­hundert­fünfzehn Komma acht sechs acht
        USA: nine hundred fifteen point eight six eight
        Deutschland: 80 %
        USA: 80%
        Deutschland: 15.897.915,869
        USA: 15,897,915.869

Datum / Uhrzeit:
        Deutschland: 20. Dezember 2014 08:37:59
        USA: December 20, 2014 at 8:37:59 AM
        Deutschland: 20. Dezember 2014
        USA: December 20, 2014
        Deutschland: 08:37
        USA: 8:37 AM

Übersetzung in deutsch:
        gestern
        5 Minuten später
        in einer Minute
        Wilkommen Rene Dziuba

Übersetzung in englisch:
        yesterday
        5 minutes ago
        in a minute
        Welcome Rene Dziuba
```