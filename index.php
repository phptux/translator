<?php
// Include-Pfad setzen
set_include_path(
    __DIR__ . '/Redi' . PATH_SEPARATOR .
    get_include_path()
);

// Autoload
spl_autoload_register(function($class) {
    require sprintf('%s.php', str_replace('\\', DIRECTORY_SEPARATOR, $class));
});

$formatterCurrency = new \Redi\I18n\Formatters\IntlFormatter(\Redi\I18n\Formatters\IntlFormatter::CURRENCY);
echo "Währungen:\n";
echo "\tDeutschland: " . $formatterCurrency->formatCurrency('15.86', 'EUR') . "\n";
echo "\tÖsterreich: " . $formatterCurrency->formatCurrency('15.86', 'EUR', TRUE, 'de_AT') . "\n";
echo "\tEngland: " . $formatterCurrency->formatCurrency('15.86', 'GBP', TRUE, 'en_GB') . "\n";
echo "\tUSA: " . $formatterCurrency->formatCurrency('15.86', 'USD', TRUE, 'en_US') . "\n";
echo "\nGewicht:\n";
echo "\tDeutschland: " . $formatterCurrency->formatCurrency('15.86', 'EUR', null, 'de_DE', '#0.# kg') . "\n";
unset($formatterCurrency);

$formatterNumber = new \Redi\I18n\Formatters\IntlFormatter(\Redi\I18n\Formatters\IntlFormatter::NUMBER);
echo "\nZahlenwerte:\n";
echo "\tDeutschland: " . $formatterNumber->formatNumber('15897915.868901234', null, null, null, 'de_DE') . "\n";
echo "\tUSA: " . $formatterNumber->formatNumber('15897915.868901234', null, null, null, 'en_US') . "\n";
echo "\tDeutschland: " . $formatterNumber->formatNumber('915.868', null, \NumberFormatter::SPELLOUT, null, 'de_DE') . "\n";
echo "\tUSA: " . $formatterNumber->formatNumber('915.868', null, \NumberFormatter::SPELLOUT, null, 'en_US') . "\n";
echo "\tDeutschland: " . $formatterNumber->formatNumber('0.8', null, \NumberFormatter::PERCENT, null, 'de_DE') . "\n";
echo "\tUSA: " . $formatterNumber->formatNumber('0.8', null, \NumberFormatter::PERCENT, null, 'en_US') . "\n";
echo "\tDeutschland: " . $formatterNumber->formatNumber('15897915.868901234', null, \NumberFormatter::DECIMAL, null, 'de_DE') . "\n";
echo "\tUSA: " . $formatterNumber->formatNumber('15897915.868901234', null, \NumberFormatter::DECIMAL, null, 'en_US') . "\n";
unset($formatterNumber);

$formatterDate = new \Redi\I18n\Formatters\IntlFormatter(\Redi\I18n\Formatters\IntlFormatter::DATE);
echo "\nDatum / Uhrzeit:\n";
echo "\tDeutschland: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::LONG, IntlDateFormatter::MEDIUM, 'de_DE') . "\n";
echo "\tUSA: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::LONG, IntlDateFormatter::MEDIUM, 'en_US') . "\n";
echo "\tDeutschland: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'de_DE') . "\n";
echo "\tUSA: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'en_US') . "\n";
echo "\tDeutschland: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::NONE, IntlDateFormatter::SHORT, 'de_DE') . "\n";
echo "\tUSA: " . $formatterDate->formatDate(new \DateTime(), IntlDateFormatter::NONE, IntlDateFormatter::SHORT, 'en_US') . "\n";
unset($formatterDate);

$t = new \Redi\I18n\Translation\Translator();
$t->setFallbackLocale('en_US')
    ->setFilePath([__DIR__ . '/i18n'])
    ->setAdapter('gettext') // `php` für PHP-Dateien
    ->setLocale('de_DE');

echo "\nÜbersetzung in deutsch:\n";
echo "\t" . $t->__('humanizer', 'yesterday') . "\n";
echo "\t" . $t->__('humanizer', 'a minute ago|{n} minutes ago', 5) . "\n";
echo "\t" . $t->__('humanizer', 'in a minute|in {n} minutes', 1) . "\n";
echo "\t" . $t->__('humanizer', 'Welcome {:username}', ['username' => 'Rene Dziuba']) . "\n";

$t->setLocale('en_US');
echo "\nÜbersetzung in englisch:\n";
echo "\t" . $t->__('humanizer', 'yesterday') . "\n";
echo "\t" . $t->__('humanizer', 'a minute ago|{n} minutes ago', 5) . "\n";
echo "\t" . $t->__('humanizer', 'in a minute|in {n} minutes', 1) . "\n";
echo "\t" . $t->__('humanizer', 'Welcome {:username}', ['username' => 'Rene Dziuba']) . "\n";