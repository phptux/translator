<?php
return [
    // Pluralformel
    ''                             => ['plural_forms' => 'nplurals=2; plural=n==1 ? 0 : 1;'],

    'yesterday'                    => 'gestern',
    'today'                        => 'heute',
    'tomorrow'                     => 'morgen',
    'a minute ago|{n} minutes ago' => 'eine Minute später|{n} Minuten später',
    'in a minute|in {n} minutes'   => 'in einer Minute|in {n} Minuten',
    'Welcome {:username}'          => 'Wilkommen {:username}'
];