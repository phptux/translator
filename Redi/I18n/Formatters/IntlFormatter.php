<?php
namespace Redi\I18n\Formatters;

use IntlDateFormatter;
use Locale;
use NumberFormatter;
use Redi\I18n\Exceptions\FormattersException;

/**
 * Formatiert Datum-Zeitangaben, Währungen und Nummern in die gewünschte Sprache.
 *
 * <b>Benötigt die PHP-Erweiterung `intl`!</b>
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Formatters
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class IntlFormatter
{
    /**
     * Konstante für Währung
     *
     * @var string
     */
    const CURRENCY = 'CURRENCY';

    /**
     * Konstante für eine Dtum-Zeit-Angabe
     *
     * @var string
     */
    const DATE = 'DATE';

    /**
     * Konstante für ein Zahl
     *
     * @var string
     */
    const NUMBER = 'NUMBER';

    /**
     * Array mit Format-Callbacks
     *
     * @var NumberFormatter[]|IntlDateFormatter[]
     */
    private static $_formatters = [];

    /**
     * Sprachlokale
     *
     * @var string
     */
    protected $locale = 'de_DE';

    /**
     * Zeitzone
     *
     * @var string
     */
    protected $timezone;

    /**
     * Pattern für die Ausgabe
     *
     * @var string
     *
     * @link http://www.php.net/manual/de/numberformatter.setpattern.php
     */
    protected $pattern;

    /**
     * 3stelliger Währungscode nach ISO 4217
     *
     * @var string
     * @link http://de.wikipedia.org/wiki/ISO_4217#Aktuell_gültige_Währungen
     */
    protected $currencyCode;

    /**
     * Format für numerische Ausgaben
     *
     * @var int
     *
     * @link http://www.php.net/manual/en/class.numberformatter.php#intl.numberformatter-constants.unumberformatstyle
     */
    protected $numberStyle = NumberFormatter::DECIMAL;

    /**
     * Formattyp für numerische Ausgaben.
     *
     * @var int
     *
     * @link http://www.php.net/manual/en/class.numberformatter.php#intl.numberformatter-constants.unumberformatstyle
     */
    protected $numberType = NumberFormatter::TYPE_DEFAULT;

    /**
     * Interne Variable - Typ der Formatierung (s. Klassenkonstante)
     *
     * @var string
     */
    private $_type;

    /**
     * Konstruktor.
     *
     * @param string $type Formattyp   (CURRENCY, DATE, NUMBER)
     *
     * @throws \Redi\I18n\Exceptions\FormattersException
     */
    public function __construct($type)
    {
        if (!extension_loaded('intl')) {
            throw new FormattersException('"%s" component requires the `intl` PHP extension.', __NAMESPACE__);
        }

        $this->_checkType($type);
    }

    /**
     * Formatiert den gewünschten Wert.<br>
     * Alias-Methode für `format()`
     *
     * @param mixed $input Wert, welcher zu formatieren ist
     *
     * @return string   formatierter Wert
     */
    public function __invoke($input)
    {
        if (self::CURRENCY === $this->_type) {
            return call_user_func_array([$this, 'formatCurrency'], func_get_args());
        }

        if (self::DATE === $this->_type) {
            return call_user_func_array([$this, 'formatDate'], func_get_args());
        }

        if (self::NUMBER === $this->_type) {
            return call_user_func_array([$this, 'formatNumber'], func_get_args());
        }

        return NULL;
    }

    /**
     * Interne Methode - Prüft auf gültigen Formattyp.
     *
     * @param string $type Formattyp
     *
     * @throws \Redi\I18n\Exceptions\FormattersException
     */
    private function _checkType($type)
    {
        $type = strtoupper($type);
        if ($type === self::CURRENCY || $type === self::DATE || $type === self::NUMBER) {
            $this->_type = $type;
        } else {
            $allowed = [self::CURRENCY, self::DATE, self::NUMBER];
            throw new FormattersException(
                'Format type "%s" is invalid. Valid types: "%s"',
                [$type, implode(', ', $allowed)]
            );
        }
    }

    /**
     * Setzt den 3stellingen Währungscode.
     *
     * @param string $currencyCode Währungscode
     *
     * @return IntlFormatter
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Gibt den 3stelligen Währungscode zurück.
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Setzt die gewünschte Sprachlokale.
     *
     * @param \Locale|string $locale
     *
     * @return IntlFormatter
     */
    public function setLocale($locale)
    {
        if (is_string($locale)) {
            $locale = Locale::getDefault();
        }
        if ($locale !== $this->locale) {
            $this->currencyCode = NULL;
        }
        $this->locale = $locale;

        return $this;
    }

    /**
     * Gibt die gesetzte Sprachlokale zurück.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Setzt das Ausgabeformat für numerische Angaben.
     *
     * @param int $numberStyle Konstante der Klasse `\NumberFormatter`
     *
     * @return $this
     */
    public function setNumberStyle($numberStyle)
    {
        $this->numberStyle = $numberStyle;

        return $this;
    }

    /**
     * Gibt das Ausgabeformat für numerische Angaben zurück.
     *
     * @return int
     */
    public function getNumberStyle()
    {
        if (NULL === $this->numberStyle) {
            $this->numberStyle = NumberFormatter::DECIMAL;
        }

        return $this->numberStyle;
    }

    /**
     * Setzt den Ausgabetyp für numerische Angaben.
     *
     * @param int $numberType Konstante der Klasse `\NumberFormatter`
     *
     * @return $this
     */
    public function setNumberType($numberType)
    {
        $this->numberType = $numberType;

        return $this;
    }

    /**
     * Gibt den Ausgabetyp für numerische Angaben zurück.
     *
     * @return int
     */
    public function getNumberType()
    {
        if (NULL === $this->numberType) {
            $this->numberType = NumberFormatter::TYPE_DEFAULT;
        }

        return $this->numberType;
    }

    /**
     * Setzt die Zeitzone.
     *
     * @param string $timezone Zeitzone
     *
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        foreach (self::$_formatters as $formatter) {
            if ($formatter instanceof IntlDateFormatter) {
                $formatter->setTimeZone(new \DateTimeZone($this->timezone));
            }
        }

        return $this;
    }

    /**
     * Gibt die gesetzte Zeitzone zurück.
     *
     * @return string
     */
    public function getTimezone()
    {
        if (NULL === $this->timezone) {
            return date_default_timezone_get();
        }

        return $this->timezone;
    }

    /**
     * Setzt das Pattern für die Ausgabe.
     *
     * @param string $pattern
     *
     * @return IntlFormatter
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Gibt das Pattern für die Ausgabe zurück.
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Formatiert eine Währung.
     *
     * <code>
     * echo $this->formatCurrency('15.86', 'EUR');
     * // Sprachlokale: 'de_DE'
     * // 15,86 €
     *
     * echo $this->formatCurrency('15.86', 'EUR', TRUE, 'de_AT');
     * // € 15,86
     *
     * echo $this->formatCurrency('15.86', 'USD', TRUE, 'en_US');
     * // $15.86
     *
     * echo $this->formatCurrency('15.86', 'GBP', FALSE, 'en_GB');
     * // £16
     *
     * echo $this->formatCurrency('15.86', 'USD', TRUE, 'de_DE');
     * // 15,86 $
     *
     * echo $this->formatCurrency('15.86', 'EUR', null, 'de_DE', '#0.# kg');
     * // 15,86 kg
     * </code>
     *
     * @param int|float    $currency Währungswert
     * @param string|null  $code     Währungscode
     * @param boolean|null $decimals 2 Kommastellen anzeigen?
     * @param string|null  $locale   Sprachlokale
     * @param string|null  $pattern  Ausgabepattern
     *
     * @return string   formatierte Währung
     */
    public function formatCurrency($currency, $code = NULL, $decimals = NULL, $locale = NULL, $pattern = NULL)
    {
        // Sprachlokale
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }

        // Währungscode
        if (NULL === $code) {
            $code = $this->getCurrencyCode();
        }

        // Kommastellen
        if (NULL === $decimals) {
            $decimals = TRUE;
        }

        // Pattern
        if (NULL === $pattern) {
            $pattern = $this->getPattern();
        }

        // interne Objekt-ID
        $formaterID = md5($locale . __FUNCTION__);

        // Objekt für Sprachlokale vorhanden?
        if (!isset(self::$_formatters[$formaterID])) {
            self::$_formatters[$formaterID] = new NumberFormatter($locale, NumberFormatter::CURRENCY);
        }

        // Pattern setzen
        if ($pattern) {
            self::$_formatters[$formaterID]->setPattern($pattern);
        }

        // Kommastellen
        if ($decimals) {
            // 2 Stellen
            self::$_formatters[$formaterID]->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
        } else {
            // keine Stelle
            self::$_formatters[$formaterID]->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
        }

        // formatieren und FERTIG
        return self::$_formatters[$formaterID]->formatCurrency($currency, $code);
    }

    /**
     * Formatiert ein Datum und/oder Zeitangabe.
     *
     * <code>
     * echo $this->formatDate(
     *     new DateTime(),
     *     IntlDateFormatter::LONG, // date
     *     IntlDateFormatter::MEDIUM, // time
     *     'de_DE'
     * );
     * // 28. Januar 2014 18:33:46
     *
     * echo $this->formatDate(
     *     new DateTime(),
     *     IntlDateFormatter::MEDIUM, // date
     *     IntlDateFormatter::MEDIUM, // time
     *     'de_DE'
     * );
     * // 28.01.2014 18:33:46
     *
     * echo $this->formatDate(
     *     new DateTime(),
     *     IntlDateFormatter::LONG, // date
     *     IntlDateFormatter::NONE, // time
     *     'de_DE'
     * );
     * // 28. Januar 2014
     *
     * echo $this->formatDate(
     *     new DateTime(),
     *     IntlDateFormatter::NONE, // date
     *     IntlDateFormatter::SHORT, // time
     *     'de_DE'
     * );
     * // 18:33
     * </code>
     *
     * @param string|int|\DateTime $date       Datumsangabe
     * @param int|null             $dateFormat Datumsausgabeformat
     * @param int|null             $timeFormat Zeitausgabeformat
     * @param string|null          $locale     Sprachlokale
     *
     * @return string   formatierte Datum- Zeitangabe
     */
    public function formatDate($date, $dateFormat = NULL, $timeFormat = NULL, $locale = NULL)
    {
        // Sprachlokale
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }

        // Zeitzone
        $timezone = $this->getTimezone();

        // interne Objekt-ID
        $formatterId = md5($dateFormat . "\0" . $timeFormat . "\0" . $locale . "\0" . __FUNCTION__);

        // Objekt für Sprachlokale vorhanden?
        if (!isset(self::$_formatters[$formatterId])) {
            self::$_formatters[$formatterId] = new IntlDateFormatter($locale, $dateFormat, $timeFormat, $timezone);
        }

        // formatieren und FERTIG
        return self::$_formatters[$formatterId]->format($date);
    }

    /**
     * Formatiert eine Zahl anhand den Daten.
     *
     * <code>
     * echo $this->formatNumber('15897915.868901234', null, null, null, 'de_DE');
     * // Sprachlokale: 'de_DE'
     * // 15.897.915,869
     *
     * echo $this->formatNumber('915.868', null, \NumberFormatter::SPELLOUT, null, 'de_DE');
     * // Sprachlokale: 'de_DE'
     * // neun­hundert­fünfzehn Komma acht sechs acht
     *
     * echo $this->formatNumber('0.8', null, \NumberFormatter::PERCENT, null, 'de_DE');
     * // Sprachlokale: 'de_DE'
     * // 80 %
     *
     * echo $this->formatNumber('15897915.868901234', null, null, null, 'en_US');
     * // Sprachlokale: 'en_US'
     * // 15,897,915.869
     *
     * echo $this->formatNumber('15897915.868901234', 2, \NumberFormatter::DECIMAL, null, 'en_US');
     * // Sprachlokale: 'en_US'
     * // 15,897,915.87
     *
     * echo $this->formatNumber('915.868', null, \NumberFormatter::SPELLOUT, null, 'en_US');
     * // Sprachlokale: 'en_US'
     * // nine hundred fifteen point eight six eight
     *
     * echo $this->formatNumber('0.75', null, \NumberFormatter::PERCENT, null, 'en_US');
     * // Sprachlokale: 'en_US'
     * // 75%
     * </code>
     *
     * @param int|float   $number Wert
     * @param null|int    $digits Kommastellen (Vorgabe: max. Anzahl)
     * @param int|null    $style  Style
     * @param int|null    $type   Typ
     * @param string|null $locale Sprachlokale
     *
     * @return string
     */
    public function formatNumber($number, $digits = NULL, $style = NULL, $type = NULL, $locale = NULL)
    {
        // Sprachlokale
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }

        // Ausgabe-Style
        if (NULL === $style) {
            $style = $this->getNumberStyle();
        }

        // Ausgabe-Typ
        if (NULL === $type) {
            $type = $this->getNumberType();
        }

        // Dezimalstellen
        if (!is_int($digits) || $digits < 0) {
            $digits = NumberFormatter::MAX_INTEGER_DIGITS;
        }

        // interne Objekt-ID
        $formatterId = md5($style . "\0" . $locale . "\0" . $digits . "\0" . __FUNCTION__);

        // Objekt für Sprachlokale vorhanden?
        if (!isset(self::$_formatters[$formatterId])) {
            self::$_formatters[$formatterId] = new \NumberFormatter($locale, $style);
        }

        // Dezimalstellen
        self::$_formatters[$formatterId]->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, (int) $digits);

        // formatieren und FERTIG
        return self::$_formatters[$formatterId]->format($number, $type);
    }
}