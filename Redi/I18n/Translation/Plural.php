<?php
namespace Redi\I18n\Translation;

use Redi\I18n\Exceptions\PluralException;

/**
 * Berechnet den Plural-Wert.
 *
 * @link       http://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html#Plural-forms
 * @link       http://developer.mozilla.org/de/docs/Localization_and_Plurals
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translation
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class Plural
{
    /**
     * Gibt ein Parser-Objekt anhand der Plural-Definition zurück.
     * Beispiel einer Plural-Definition: `nplurals=2; plural=n != 1;`
     *
     * @param string $str Plural-Definition
     *
     * @throws \Redi\I18n\Exceptions\PluralException
     * @return Plural
     */
    static public function from($str)
    {
        if (!preg_match('(nplurals=(?P<nplurals>\d+))', $str, $match)) {
            throw new PluralException('Unknown or invalid parser rule: %s', $str);
        }

        $numPlurals = (int) $match['nplurals'];

        if (!preg_match('(plural=(?P<plural>[^;\n]+))', $str, $match)) {
            throw new PluralException('Unknown or invalid parser rule: %s', $str);
        }

        return new static($numPlurals, $match['plural']);
    }

    /**
     * Menge der Pluralformen
     *
     * @var int
     */
    protected $pluralForm;

    /**
     * Plural-Regel
     *
     * @var string
     */
    protected $plural;

    /**
     * PHP-Funktion zum Ausführen der Pluralregel
     *
     * @var callable
     */
    protected $pluralFunction;

    /**
     * Konstruktor.
     *
     * @param int    $forms  Anzahl der Pluralformen
     * @param string $plural Plural-Regel
     */
    public function __construct($forms, $plural)
    {
        $this->pluralForm = (int) $forms;
        $this->plural = $this->_sanitizePluralFunction($plural);

        $this->pluralFunction = create_function('$number', 'return ' . str_replace('n', '$number', $this->plural) . ';');
    }

    /**
     * Gibt die Anzahl der Pluralformen zurück.
     *
     * @return int
     */
    public function getPluralForm()
    {
        return $this->pluralForm;
    }

    /**
     * Gibt anhand der Anzahl die Nummer des Plurals in der Übersetzung zurück.
     *
     * @param int $number Anzahl
     *
     * @throws \Redi\I18n\Exceptions\PluralException
     * @return int
     */
    public function getPlural($number)
    {
        $func = $this->pluralFunction;

        $result = $func(abs((int) $number));

        if ($result < 0 || $result >= $this->pluralForm) {
            throw new PluralException('Calculated result %s is between 0 and %d', [$result, ($this->pluralForm - 1)]);
        }

        return $result;
    }

    /**
     * @param string $plural Plural-Funktion
     *
     * @return string
     */
    private function _sanitizePluralFunction($plural)
    {
        if (substr($plural, -1) !== ';') {
            $plural .= ';';
        }

        $res = '';
        $p = 0;
        for ($i = 0; $i < strlen($plural); $i++) {
            $ch = $plural[$i];
            switch ($ch) {
                case '?':
                    $res .= ' ? (';
                    $p++;
                    break;
                case ':':
                    $res .= ') : (';
                    break;
                case ';':
                    $res .= str_repeat(')', $p) . ';';
                    $p = 0;
                    break;
                default:
                    $res .= $ch;
            }
        }

        return '(' . substr($res, 0, -1) . ')';
    }
}