<?php
namespace Redi\I18n\Translation;

use Locale;
use Redi\I18n\Exceptions\TranslatorException;
use Redi\I18n\Translation\Adapters\IAdapter;

/**
 * Übersetzungsklasse.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translation
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class Translator
{
    /**
     * Array mit unterstützen Leseklassen
     *
     * @var array
     */
    public static $supportedAdapters
        = [
            'php'     => 'Redi\\I18n\\Translation\\Adapters\\PhpAdapter',
            'gettext' => 'Redi\\I18n\\Translation\\Adapters\\GettextAdapter'
        ];

    /**
     * Sprachlokale für die Übersetzung.
     *
     * @var string
     */
    protected $locale;

    /**
     * Sprachlokale als Vorgabe.
     * Diese Sprache wird NICHT übersetzt.
     *
     * @var string
     */
    protected $fallbackLocale;

    /**
     * Sprache und Region der Sprachlokalen bilden den Sprachordner?
     *
     * @var bool
     */
    protected $fullLocale = FALSE;

    /**
     * Array mit Pfaden zu den Übersetzungsdateien.
     *
     * @var array
     */
    protected $filePaths = [];

    /**
     * Daten-Array
     *
     * @var array
     */
    private $_data = [];

    /**
     * Instanz der Adapterklasse
     *
     * @var IAdapter
     */
    private $_adapter;

    /**
     * Übersetzungsfunktion.
     *
     * Benötigt das Koala-Framework-Modul `koala-i18n`.
     *
     * @param string $textDomain Text-Domain
     * @param string $text       Sprachtext
     * @param array  $replaces   Daten für Platzhalter im Sprachtext
     * @param string $locale     Sprachlokale
     *
     * @return string   übersetzter Text mit ersetzten Platzhaltern
     */
    public function __($textDomain, $text, $replaces = [], $locale = NULL)
    {
        $text = $this->translate($textDomain, $text, $locale);
        $plural = $this->getPluralRule($textDomain, $locale);

        // keine Platzhalter
        if ($replaces === []) {
            return $text;
        }

        // Platzhalter müssen ein Array sein
        if (!is_array($replaces)) {
            $replaces = [$replaces];
        }

        // Plural?
        if (isset($replaces[0])) {
            if (strpos($text, '|') !== FALSE) {
                $parts = explode('|', $text);
                $idx = $plural->getPlural($replaces[0]);
                if (!isset($parts[$idx])) {
                    $idx = ($replaces[0] != 1 ? 0 : 1);
                }
                $text = $parts[$idx];
            }
            if (!isset($replaces['{n}'])) {
                $replaces['{n}'] = $replaces[0];
            }
            unset($replaces[0]);
        }

        // Platzhalter ersetzen
        $rp = [];
        foreach ($replaces as $k => $v) {
            if (strpos($k, '{') !== 0) {
                $rp['{:' . $k . '}'] = $v;
            } else {
                $rp[$k] = $v;
            }
        }

        return (empty($rp) ? $text : strtr($text, $rp));
    }

    /**
     * Setzt einen oder mehrere Pfade zu den Übersetzungsdateien.
     *
     * @param array $filePaths Pfadangabe
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return Translator
     */
    public function setFilePath($filePaths)
    {
        if (is_array($filePaths)) {
            foreach ($filePaths as $path) {
                $this->setFilePath($path);
            }

            return $this;
        }

        $real = realpath($filePaths);
        if (!is_dir($real)) {
            throw new TranslatorException('Path "%s" for translation files not exists.', $filePaths);
        }

        $this->filePaths[] = $filePaths;

        return $this;
    }

    /**
     * Gibt alle gesetzten Pfade zu den Übersetzungsdateien zurück.
     *
     * @return array
     */
    public function getFilePaths()
    {
        return $this->filePaths;
    }

    /**
     * Setzt ein Adapter-Objekt zum auslesen der Sprachdateien.
     *
     * @param string|IAdapter $adapter
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return Translator
     */
    public function setAdapter($adapter)
    {
        if (is_object($adapter) AND !$adapter instanceof IAdapter) {
            throw new TranslatorException(
                'Class "%s" not implements "%s".', [get_class($adapter), 'koala\\i18n\\adapters\\IAdapter']
            );
        }

        if (is_string($adapter) AND !isset(self::$supportedAdapters[$adapter])) {
            throw new TranslatorException('Adapter for transltion "%s" is not supported.', $adapter);
        }

        if (is_object($adapter)) {
            $this->_adapter = $adapter;

            return $this;
        }

        $class = self::$supportedAdapters[$adapter];
        $this->_adapter = new $class();

        return $this;
    }

    /**
     * Gibt das Adapter-Objekt zum auslesen der Sprachdateien zurück.
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return IAdapter
     */
    public function getAdapter()
    {
        if (NULL === $this->_adapter) {
            throw new TranslatorException('No adapter for tanslation found.');
        }

        return $this->_adapter;
    }

    /**
     * Gibt eine neue Instanz eines benannten Adapters zurück.
     *
     * @param string $name Adaptername
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return IAdapter
     */
    public function getAdapterFromName($name)
    {
        if (!isset(self::$supportedAdapters[$name])) {
            throw new TranslatorException('Adapter for transltion "%s" is not supported.', $name);
        }
        $class = self::$supportedAdapters[$name];

        return new $class();
    }

    /**
     * Lokale, welche nicht übersetzt wird.
     *
     * @param string $fallbackLocale Sprachlokale
     *
     * @return Translator
     */
    public function setFallbackLocale($fallbackLocale)
    {
        $this->fallbackLocale = $fallbackLocale;

        return $this;
    }

    /**
     * Gibt die Sprachlokale zurück, welche nicht übersetzt wird.
     *
     * @return string
     */
    public function getFallbackLocale()
    {
        if (!$this->fullLocale) {
            $ex = explode('_', $this->fallbackLocale);

            return $ex[0];
        }

        return $this->fallbackLocale;
    }

    /**
     * Soll die ganze Sprachlokale als Unterordner genutz werden?
     *
     * @param boolean $flag
     *
     * @return Translator
     */
    public function setFullLocale($flag)
    {
        $this->fullLocale = (bool) $flag;

        return $this;
    }

    /**
     * Gibt zurück, ob die ganze Sprachlokale als Unterordner genutz wird.
     *
     * @return boolean
     */
    public function getFullLocale()
    {
        return $this->fullLocale;
    }

    /**
     * Setzt die zu benutzende Sprachlokale.
     *
     * @param string $locale Sprachlokale
     *
     * @return Translator
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Gibt die zu benutzende Sprachlokale zurück.
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return string
     */
    public function getLocale()
    {
        if ($this->locale === NULL) {
            if (!extension_loaded('intl')) {
                throw new TranslatorException('%s component requires the intl PHP extension', __NAMESPACE__);
            }
            $this->locale = Locale::getDefault();
        }

        return $this->locale;
    }

    /**
     * Gibt die Plural-Rule der Domain zurück.
     *
     * @param string      $textDomain Text-Domain
     * @param string|null $locale     Sprachlokale (optional)
     *
     * @return Plural
     */
    public function getPluralRule($textDomain, $locale = NULL)
    {
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }
        if (!$this->fullLocale) {
            $ex = explode('_', $locale);
            $locale = $ex[0];
        }

        if (isset($this->_data[$textDomain]) AND isset($this->_data[$textDomain][$locale])) {
            /** @var $domain Domain */
            $domain = $this->_data[$textDomain][$locale];

            return $domain->getPluralRule();
        }

        $domain = new Domain();

        return $domain->getPluralRule();
    }

    /**
     * Gibt den übersetzten Text zurück.
     *
     * @param string $textDomain Text-Domain
     * @param string $text       Sprachtext
     * @param string $locale     Sprachlokale
     *
     * @return string
     */
    public function translate($textDomain, $text, $locale = NULL)
    {
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }
        if (!$this->fullLocale) {
            $ex = explode('_', $locale);
            $locale = $ex[0];
        }

        $translation = $this->getMessage($textDomain, $text, $locale);

        if ($translation !== NULL AND $translation !== '') {
            if (is_array($translation)) {
                return $this->translatePlural($textDomain, $text, '', 1, $locale);
            }

            return $translation;
        }

        if (NULL !== ($fallbackLocale = $this->getFallbackLocale()) AND $locale !== $fallbackLocale) {
            return $this->translate($textDomain, $text, $fallbackLocale);
        }

        return $text;
    }

    /**
     * Übersetzt einen Plural-Text.
     *
     * @param string      $textDomain Text-Domain
     * @param string      $singular   Text Einzahl
     * @param string      $plural     Text Plural
     * @param string      $number     Anzahl
     * @param string|null $locale     Sprachlokale
     *
     * @throws \Redi\I18n\Exceptions\TranslatorException
     * @return string
     */
    public function translatePlural($textDomain, $singular, $plural, $number, $locale = NULL)
    {
        if (NULL === $locale) {
            $locale = $this->getLocale();
        }
        if (!$this->fullLocale) {
            $ex = explode('_', $locale);
            $locale = $ex[0];
        }

        $translation = $this->getMessage($textDomain, $singular, $locale);

        if ($translation === NULL || $translation === '') {
            if (NULL !== ($fallbackLocale = $this->getFallbackLocale()) AND $locale !== $fallbackLocale) {
                return $this->translatePlural($textDomain, $singular, $plural, $number, $fallbackLocale);
            }

            return ($number == 1 ? $singular : $plural);
        }

        /** @var $domain Domain */
        $domain = $this->_data[$textDomain][$locale];
        $idx = $domain->getPluralRule()->getPlural($number);

        if (!isset($translation[$idx])) {
            throw new TranslatorException('Provided index %d does not exist in plural array.', $idx);
        }

        return $translation[$idx];
    }

    /**
     * Gibt den übersetzten Text zurück.
     *
     * @param string $textDomain Name der Text-Domain
     * @param string $text       zu übersetzender Text
     * @param string $locale     Sprachlokale
     *
     * @return string
     */
    protected function getMessage($textDomain, $text, $locale)
    {
        if ($text === '') {
            return '';
        }

        if (!isset($this->_data[$textDomain])) {
            $this->_data[$textDomain] = [
                $locale => $this->loadMessages($textDomain, $locale)
            ];

        }

        if (!isset($this->_data[$textDomain][$locale])) {
            $this->_data[$textDomain][$locale] = $this->loadMessages($textDomain, $locale);
        }

        if (isset($this->_data[$textDomain][$locale][$text])) {
            return $this->_data[$textDomain][$locale][$text];
        }

        return NULL;
    }

    /**
     * Lädt die Übersetzungsdaten für eine Text-Domain.
     *
     * @param string $textDomain Name der Text-Domain
     * @param string $locale     Sprachlokale
     *
     * @return Domain
     */
    protected function loadMessages($textDomain, $locale)
    {
        return $this->getAdapter()->load($this->getFilePaths(), $textDomain, $locale);
    }
}