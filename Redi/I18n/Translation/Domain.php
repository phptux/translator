<?php
namespace Redi\I18n\Translation;

use ArrayObject;
use Redi\I18n\Exceptions\DomainException;

/**
 * Verwaltet die Übersetzungstexte einer Domain bzw. einer Datei.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translation
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class Domain extends ArrayObject
{
    /**
     * Objekt der Plural-Klasse
     *
     * @var Plural
     */
    protected $pluralRule;

    /**
     * Setzt das Plural-Objekt für die Domain.
     *
     * @param Plural $pluralRule
     *
     * @return Domain
     */
    public function setPluralRule(Plural $pluralRule)
    {
        $this->pluralRule = $pluralRule;

        return $this;
    }

    /**
     * Gibt das Plural-Objekt für die Domain zurück.
     *
     * @return Plural
     */
    public function getPluralRule()
    {
        if (NULL === $this->pluralRule) {
            // englische Vorgabe wird gesetzt
            $this->setPluralRule(Plural::from('nplurals=2; plural=n==1 ? 0 : 1;'));
        }

        return $this->pluralRule;
    }

    /**
     * Verbindet die Daten einer anderen Domain mit der derselben.
     *
     * @param Domain $domain Domain-Objekt
     *
     * @throws \Redi\I18n\Exceptions\DomainException
     * @return Domain
     */
    public function merge(Domain $domain)
    {
        if ($this->getPluralRule()->getPluralForm() !== $domain->getPluralRule()->getPluralForm()) {
            throw new DomainException('Plural rule of merging text domain is not compatible with the current one.');
        }

        $this->exchangeArray(array_replace($this->getArrayCopy(), $domain->getArrayCopy()));

        return $this;
    }
}