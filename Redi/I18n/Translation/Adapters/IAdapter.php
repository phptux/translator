<?php
namespace Redi\I18n\Translation\Adapters;

use Redi\I18n\Translation\Domain;

/**
 * Interface für alle Adapter-Klassen.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translation\Adapters
 * @author     Rene Dziuba
 * @since      1.0.0
 */
interface IAdapter
{
    /**
     * Gibt den Inhalt der Übersetzungsdatei als ein Domain-Objekt zurück.
     *
     * @param array  $paths  Pfade zu den Übersetzungsdateien
     * @param string $file   Name der Übersetzungsdatei
     * @param string $locale Sprachlokale
     *
     * @return Domain
     */
    public function load($paths, $file, $locale);
}