<?php
namespace Redi\I18n\Translation\Adapters;

/**
 * Liest Gettext-po-Dateien ein.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translations\Adapters
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class GettextPoAdapter
{
    /**
     * Gibt den Inhalt der .po-Übersetzungsdatei als Array zurück.
     *
     * @param string $file Name der Übersetzungsdatei mit absulter Pfadangabe
     *
     * @return array
     */
    static public function load($file)
    {
        $messages = [];

        if (file_exists($file)) {
            $content = file_get_contents($file);
        } else {
            return $messages;
        }

        $pattern = '/msgid\s+((?:".*(?<!\\\\)"\s*)+)\s+'
            . 'msgstr\s+((?:".*(?<!\\\\)"\s*)+)/';

        $n = preg_match_all($pattern, $content, $matches);

        for ($i = 0; $i < $n; ++$i) {
            $id = self::_decode(substr(rtrim($matches[1][$i]), 1, -1));
            $message = self::_decode(substr(rtrim($matches[2][$i]), 1, -1));
            $messages[$id] = $message;
        }

        // Plural
        preg_match('/(?<=Plural-Forms: ).*(?=\n)/', $content, $plural);
        if (isset($plural[0])) {
            $messages[''] = ['plural_form' => $plural[0]];
        }

        return $messages;
    }

    /**
     * Dekodiert spezielle Zeichen in einem Übersetzungstext.
     *
     * @param string $string Übersetzungstext
     *
     * @return string dekodierter Übersetzungstext
     */
    static private function _decode($string)
    {
        return str_replace(['\\"', "\\n", '\\t', '\\r'], ['"', "\n", "\t", "\r"], $string);
    }

    /**
     * Kontruktoraufruf unterbinden.
     * (Statische-Klasse)
     */
    final private function __construct()
    {
    }
}