<?php
namespace Redi\I18n\Translation\Adapters;

use Redi\I18n\Translation\Domain;
use Redi\I18n\Translation\Plural;

/**
 * Leseklasse für Gettext-Dateien.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translations\Adapters
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class GettextAdapter implements IAdapter
{
    /**
     * Gibt den Inhalt der Übersetzungsdatei als ein Domain-Objekt zurück.
     *
     * @param array  $paths  Pfade zu den Übersetzungsdateien
     * @param string $file   Name der Übersetzungsdatei
     * @param string $locale Sprachlokale
     *
     * @return Domain
     *
     * @todo Read gettxet .mo-files
     */
    public function load($paths, $file, $locale)
    {
        $file = DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . $file;

        foreach ($paths as $path) {
            // .po-Dateien
            if (file_exists($path . $file . '.po')) {
                $messages = GettextPoAdapter::load($path . $file . '.po');
                break;
                // .mo-Dateien
            } elseif (file_exists($path . $file . '.mo')) {
                // @todo load .mo-files
                //$messages = Files::includeFile($path . $file);
                break;
            }
        }

        if (!isset($messages)) {
            return new Domain();
        }

        $domain = new Domain($messages);

        if (array_key_exists('', $domain)) {
            if (isset($domain['']['plural_forms'])) {
                $domain->setPluralRule(Plural::from($domain['']['plural_forms']));
            }
            unset($domain['']);
        }

        return $domain;
    }
}