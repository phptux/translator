<?php
namespace Redi\I18n\Translation\Adapters;

use Redi\I18n\Translation\Domain;
use Redi\I18n\Translation\Plural;

/**
 * Liest die Übersetzung aus einem Php-Array.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Translation\Adapters
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class PhpAdapter
{
    /**
     * Gibt den Inhalt der Übersetzungsdatei als ein Domain-Objekt zurück.
     *
     * @param array  $paths  Pfade zu den Übersetzungsdateien
     * @param string $file   Name der Übersetzungsdatei
     * @param string $locale Sprachlokale
     *
     * @return Domain
     */
    public function load($paths, $file, $locale)
    {
        $file = DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . $file . '.php';

        foreach ($paths as $path) {
            if (file_exists($path . $file)) {
                $messages = include($path . $file);
                break;
            }
        }

        if (!isset($messages)) {
            return new Domain();
        }

        $domain = new Domain($messages);

        if (array_key_exists('', $domain)) {
            if (isset($domain['']['plural_forms'])) {
                $domain->setPluralRule(Plural::from($domain['']['plural_forms']));
            }
            unset($domain['']);
        }

        return $domain;
    }
}