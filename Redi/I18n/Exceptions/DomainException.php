<?php
namespace Redi\I18n\Exceptions;

/**
 * Diese Ausnahme wird geworfen, wenn ein Fehler in der
 * Klasse `Redi\I18n\Translation\Domain` auftritt.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Exceptions
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class DomainException extends TranslateExceptionText
{

}