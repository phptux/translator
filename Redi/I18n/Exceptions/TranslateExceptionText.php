<?php
/**
 * Part of the Redi framework.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT license that is bundled
 * with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * http://www.mrversion.de/license-mit.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to <redi.license at mrversion dot de> so we can send you a copy immediately.
 *
 * @link       https://bitbucket.org/phptux/redi-I18n.strtolover() for the canonical source repository
 * @copyright  Copyright (c) 2006 - 2014 Mrversion (http://www.mrversion.de)
 * @license    http://www.mrversion.de/license-mit.txt    MIT License
 */
namespace Redi\I18n\Exceptions;

use Exception;

/**
 * Übersetzungsfunktion für die Fehlertext in einer Exception.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Exceptions
 * @author     Rene Dziuba
 * @since      1.0.0
 */
abstract class TranslateExceptionText extends Exception
{
    /**
     * Konstruktor.
     *
     * @param string      $message  Fehlermeldung
     * @param array|mixed $replaces Array mit Platzhaltern in der Fehlermeldung
     * @param int         $code     Fehlercode
     * @param \Exception  $prev     vorausgegangene Exception
     */
    public function __construct($message, $replaces = [], $code = 0, \Exception $prev = NULL)
    {
        // Platzhalter
        if ($replaces !== NULL) {
            if (!is_array($replaces)) {
                $replaces = [$replaces];
            }
            $message = vsprintf($message, $replaces);
        }

        // Konstruktor
        parent::__construct($message, $code, $prev);
    }
}