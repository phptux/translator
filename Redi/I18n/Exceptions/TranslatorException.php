<?php
/**
 * Part of the Redi framework.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT license that is bundled
 * with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * http://www.mrversion.de/license-mit.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to <redi.license at mrversion dot de> so we can send you a copy immediately.
 *
 * @link       https://bitbucket.org/phptux/redi-redi for the canonical source repository
 * @copyright  Copyright (c) 2006 - 2014 Mrversion (http://www.mrversion.de)
 * @license    http://www.mrversion.de/license-mit.txt    MIT License
 */
namespace Redi\I18n\Exceptions;

/**
 * Diese Ausnahme wird geworfen, wenn ein Fehler in der
 * Klasse `Redi\I18n\Translation\Transtaltor` auftritt.
 *
 * @category   Redi
 * @package    I18n
 * @subpackage Exceptions
 * @author     Rene Dziuba
 * @since      1.0.0
 */
class TranslatorException extends TranslateExceptionText
{

}